const menu = [
  {
    id: 1,
    category: "cat1",
    title: "category 1",
    img: "img/1.jpg",
    desc: "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quaerat non repellat veritatis saepe dolore nostrum, cum qui optio velit!",
    prise: 60,
  },
  {
    id: 2,
    category: "cat2",
    title: "category 2",
    img: "img/2.jpg",
    desc: "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quaerat non repellat veritatis saepe dolore nostrum, cum qui optio velit!",
    prise: 40,
  },
  {
    id: 3,
    category: "cat3",
    title: "category 3",
    img: "img/3.jpg",
    desc: "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quaerat non repellat veritatis saepe dolore nostrum, cum qui optio velit!",
    prise: 20,
  },
  {
    id: 4,
    category: "cat4",
    title: "category 4",
    img: "img/4.jpg",
    desc: "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quaerat non repellat veritatis saepe dolore nostrum, cum qui optio velit!",
    prise: 101,
  },
  {
    id: 5,
    category: "cat5",
    title: "category 5",
    img: "img/5.jpg",
    desc: "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quaerat non repellat veritatis saepe dolore nostrum, cum qui optio velit!",
    prise: 120,
  },
  {
    id: 3,
    category: "cat4",
    title: "Projects",
    img: "img/3.jpg",
    desc: "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quaerat non repellat veritatis saepe dolore nostrum, cum qui optio velit!",
    prise: 20,
  },
  {
    id: 4,
    category: "cat2",
    title: "Books",
    img: "img/4.jpg",
    desc: "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quaerat non repellat veritatis saepe dolore nostrum, cum qui optio velit!",
    prise: 101,
  },
  {
    id: 5,
    category: "cat5",
    title: "Food",
    img: "img/5.jpg",
    desc: "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quaerat non repellat veritatis saepe dolore nostrum, cum qui optio velit!",
    prise: 120,
  },
];
const wrapper = document.querySelector(".menu_wrapper");
const btns = document.querySelectorAll(".btn");

// lodaed
window.addEventListener("DOMContentLoaded", function () {
  displayCategory(menu);
});
// elemnts
function displayCategory(menuElems) {
  let menuItems = menuElems.map(function (item) {
    return `<div class="menu_widget">
              <div class="thumb">
                  <img src="${item.img}" alt="">
              </div>
              <h3 class="title_text">${item.title}</h3>
              <p class="desc_text">${item.desc}</p>
              <p class="prise">$${item.prise}</p>
          </div>`;
  });
  menuItems = menuItems.join("");
  wrapper.innerHTML = menuItems;
}
// filters
btns.forEach(function (btn) {
  btn.addEventListener("click", function (e) {
    const CateGory = e.currentTarget.dataset.id;
    const filterCategory = menu.filter(function (menuItem) {
      if (menuItem.category === CateGory) {
        return menuItem;
      }
    });
    if (CateGory === "all") {
      displayCategory(menu);
    } else {
      displayCategory(filterCategory);
    }
  });
});
