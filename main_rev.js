const menu = [
  {
    id: 1,
    category: "cat1",
    title: "category 1",
    img: "img/1.jpg",
    desc: "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quaerat non repellat veritatis saepe dolore nostrum, cum qui optio velit!",
    prise: 60,
  },
  {
    id: 2,
    category: "cat2",
    title: "category 2",
    img: "img/2.jpg",
    desc: "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quaerat non repellat veritatis saepe dolore nostrum, cum qui optio velit!",
    prise: 40,
  },
  {
    id: 3,
    category: "cat3",
    title: "category 3",
    img: "img/3.jpg",
    desc: "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quaerat non repellat veritatis saepe dolore nostrum, cum qui optio velit!",
    prise: 20,
  },
  {
    id: 4,
    category: "cat4",
    title: "category 4",
    img: "img/4.jpg",
    desc: "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quaerat non repellat veritatis saepe dolore nostrum, cum qui optio velit!",
    prise: 101,
  },
  {
    id: 5,
    category: "cat5",
    title: "category 5",
    img: "img/5.jpg",
    desc: "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quaerat non repellat veritatis saepe dolore nostrum, cum qui optio velit!",
    prise: 120,
  },
  {
    id: 3,
    category: "cat4",
    title: "Projects",
    img: "img/3.jpg",
    desc: "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quaerat non repellat veritatis saepe dolore nostrum, cum qui optio velit!",
    prise: 20,
  },
  {
    id: 4,
    category: "cat2",
    title: "Books",
    img: "img/4.jpg",
    desc: "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quaerat non repellat veritatis saepe dolore nostrum, cum qui optio velit!",
    prise: 101,
  },
  {
    id: 5,
    category: "cat5",
    title: "Food",
    img: "img/5.jpg",
    desc: "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quaerat non repellat veritatis saepe dolore nostrum, cum qui optio velit!",
    prise: 120,
  },
  {
    id: 5,
    category: "cat5",
    title: "Food",
    img: "img/5.jpg",
    desc: "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quaerat non repellat veritatis saepe dolore nostrum, cum qui optio velit!",
    prise: 120,
  },
  {
    id: 5,
    category: "cat3",
    title: "Food",
    img: "img/5.jpg",
    desc: "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quaerat non repellat veritatis saepe dolore nostrum, cum qui optio velit!",
    prise: 120,
  },
  {
    id: 5,
    category: "cat1",
    title: "Food",
    img: "img/5.jpg",
    desc: "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quaerat non repellat veritatis saepe dolore nostrum, cum qui optio velit!",
    prise: 120,
  },
  {
    id: 5,
    category: "cat6",
    title: "Food",
    img: "img/5.jpg",
    desc: "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quaerat non repellat veritatis saepe dolore nostrum, cum qui optio velit!",
    prise: 120,
  },
  {
    id: 5,
    category: "cat7",
    title: "Food",
    img: "img/5.jpg",
    desc: "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quaerat non repellat veritatis saepe dolore nostrum, cum qui optio velit!",
    prise: 120,
  },
];
let menu_wrapper = document.querySelector(".menu_wrapper");
let buton_container = document.querySelector(".buton_container");
window.addEventListener("DOMContentLoaded", function () {
  displayFunction(menu);
  buttonFunction();
});

// ctrating elemnt
function displayFunction(gettingElement) {
  let displayElements = gettingElement.map(function (item) {
    return `<div class="menu_widget">
            <div class="thumb">
                <img src="${item.img}" alt="">
            </div>
            <h3 class="title_text">${item.title}</h3>
            <p class="desc_text">${item.desc}</p>
            <p class="prise">$${item.prise}</p>
        </div>`;
  });
  displayElements = displayElements.join("");
  menu_wrapper.innerHTML = displayElements;
}
//
//
//
//
// buttonFunction
function buttonFunction() {
  let selectCat = menu.reduce(
    function (values, item) {
      if (!values.includes(item.category)) {
        values.push(item.category);
      }
      return values;
    },
    ["all"]
  );
  let buttonCat = selectCat.map(function (cat) {
    return `<button data-id="${cat}" class="btn">${cat}</button>`;
  });
  buttonCat = buttonCat.join("");

  buton_container.innerHTML = buttonCat;
  let btns = document.querySelectorAll(".btn");
  btns.forEach(function (btn) {
    btn.addEventListener("click", function (e) {
      let currentCategory = e.currentTarget.dataset.id;
      let categoryItem = menu.filter(function (item) {
        if (item.category === currentCategory) {
          console.log(item);
          return item;
        }
      });
      if (currentCategory == "all") {
        displayFunction(menu);
      } else {
        displayFunction(categoryItem);
      }
    });
  });
}
